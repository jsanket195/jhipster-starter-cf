/**
 * Data Access Objects used by WebSocket services.
 */
package com.sanky.jhipsterstarterpgsql.web.websocket.dto;
