/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sanky.jhipsterstarterpgsql.web.rest.vm;
