(function() {
    'use strict';

    angular
        .module('jhipsterStarterPgsqlApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
