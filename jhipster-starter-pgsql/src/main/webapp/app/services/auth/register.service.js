(function () {
    'use strict';

    angular
        .module('jhipsterStarterPgsqlApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
